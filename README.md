# THIS PROJECT HAS BEEN DEPRECATED IN FAVOR OF [ISLEWARD-CLIENT](https://gitlab.com/Isleward/isleward-client)

# Isleward Desktop App

This is a desktop app for Isleward developed using Electron.

Currently it can render and play the game, store a couple of settings and has a developer console. Support for more features, including [Discord Rich Presence](https://discordapp.com/rich-presence) is planned.

## Can I download the app?

Binaries are largely untested, however, they can be found under [Tags](https://gitlab.com/vildravn/isleward-electron/tags) or on [Google Drive](https://drive.google.com/drive/folders/1xGxUg6tB0eVa-Ivfj7YgDFl3dJEFR6jg?usp=sharing) directly. (If you know of a better file host, tell me please)

## What do I do if I find a bug?

Please report any issues related to the electron binary itself [here](https://gitlab.com/vildravn/isleward-electron/issues).  
For ingame issues, go to the [Isleward repository](https://gitlab.com/Isleward/isleward/issues).

## How do I change my widow size?

Currently, window size defaults to 1920x1080 but if you are brave enough, you can change the `Settings` file located in:
* Windows - `%APPDATA%/isleward`
* MacOS - `~/Library/Application Support/isleward`
* Linux - `$XDG_CONFIG_HOME/isleward` or `~/.config/isleward`