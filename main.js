const electron = require('electron')
const settings = require('electron-settings')
// Module to control application life.
const app = electron.app
const globalShortcut = electron.globalShortcut
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const path = require('path')
const url = require('url')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

// globals for options
let winWidth
let winHeight
let iwdUrl

// Loads settings from a file, if some are unset, use default values
function loadSettings () {
    if (!settings.has('window.width')) {
        settings.set('window.width', 1920)
    }

    if (!settings.has('window.height')) {
        settings.set('window.height', 1080)
    }

    if (!settings.has('islewardUrl')) {
        settings.set('islewardUrl', 'play.isleward.com')
    }

    winWidth = settings.get('window.width')
    winHeight = settings.get('window.height')
    iwdUrl = settings.get('islewardUrl')
}

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
      title: 'Isleward',
      width: winWidth,
      height: winHeight,
      backgroundColor: '#2D2136',
      icon: 'favicon.ico',
      autoHideMenuBar: true
    })

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: iwdUrl,
    protocol: 'http:',
    slashes: true
  }))

  mainWindow.on('page-title-updated', function(event) {
      // Prevent the website from updating the window title
      event.preventDefault()
  })

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })

  mainWindow.setMenu(null)
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function () {
    // Load settings from a file
    loadSettings()

    // Open Console on Ctrl+Shift+I
    globalShortcut.register('CmdOrCtrl+Shift+I', function () {
        mainWindow.webContents.openDevTools({mode: "detach"})
    })
    createWindow()
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
